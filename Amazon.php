<?php

	namespace Scrapper;
	
	use Exception;

	class Amazon
	{
		/**
		This is the simple Amazon class to scrape any Amazon's UK or US product page
		**/
		public function __construct($iso3166_1Alpha2CountryCode)
		{
			/**
			Initialize the class assigning the country_tld code
			Parameters:
			- country_tld: country code to geolocally the url
			**/
			
			$this->country_tld = $iso3166_1Alpha2CountryCode;
		}

		public function getItem($asin)
		{
			/**
			It returns real and live data against any ASIN requested into an array.
			Parameters:
			- url: it builds the url to reach the webpage
			- items: it calls the method _getHtmlStringFromUrl() to get webpage html content 
			- results : array where to save the data retrieved
			- asin: parameter passed to this method to construct the url.
			**/
			try {
				$url = $this->item = 'http://www.amazon.com/'.$this->country_tld.'/dp/'.$asin.'/';
				//var_dump($url);
				
				$items = $this->_getHtmlStringFromUrl($url);
				
				$results = [];
				$results['title'] = $items->query('//h1[@id="title"]');				
				$results['price'] = $items->query('//span[@id="price_inside_buybox"]');
				$results['imageUrl'] = $items->query('//img[@id="landingImage"]/@src');  ////img/@src
				 				  
				foreach ($results['title'] as $result) { 
					$results['title'] = ($result->nodeValue); 
				}
				
				foreach ($results['price'] as $result) { 
					$results['price'] = ($result->nodeValue);
				}

				foreach ($results['imageUrl'] as $result) { 
					$results['imageUrl'] = ($result->nodeValue);
				}
				
				//print_r($results);
				
			}
			catch(Exception $e) {	
		
				throw new Exception('Expected exception');
			}	
			finally {
				return $results;
			}
		}

		protected function _getHtmlStringFromUrl($url)
		{
			/**
			It triggers a call to the web page, taking the data to be processed later.
			Parameters:
			- ch: to save the data throw the cUrl process
			- content: variable to save the data obtained from the webpage
			**/
			
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 80);
			
			$content = curl_exec($ch);
			if(curl_errno($ch)) // check for execution errors
			{
				echo 'Scraper error: ' . curl_error($ch);
				exit;
			}
			
			curl_close($ch);
			
			$xpath = $this->_getDOMXpathFromHtmlString($content);
						
			return $xpath;
		}

		protected function _getDOMXpathFromHtmlString($htmlString)
		{
			$domDoc = new \DOMDocument(); 

			@$domDoc->loadHtml('<?xml encoding="UTF-8">' .$htmlString);

			return new \DOMXPath( $domDoc ); 
		}
	}

?>