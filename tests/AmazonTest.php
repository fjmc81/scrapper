<?php
	
	use PHPUnit\Framework\TestCase;
	//use Tests\TestCase;

	final class AmazonTest extends TestCase
	{
		public function testExistingGbExtract()
	    {
	        $scrapper 	= new scrapper\Amazon('GB');
	        $item 		= $scrapper->getItem('B07NNRTTCM'); //ASIN updated
	        
	        $this->assertIsArray($item);
	        // $this->assertInternalType('array',$item);

	        $this->assertArrayHasKey('title', $item);
	        $this->assertArrayHasKey('price', $item);
	        $this->assertArrayHasKey('imageUrl', $item);
	    }

	    public function testExistingUsExtract()
	    {
	        $scrapper 	= new scrapper\Amazon('US');
	        $item 		= $scrapper->getItem('B078DPCY3T'); //ASIN updated

	        $this->assertIsArray($item);
	        // $this->assertInternalType('array',$item);

	        $this->assertArrayHasKey('title', $item);
	        $this->assertArrayHasKey('price', $item);
	        $this->assertArrayHasKey('imageUrl', $item);
	    }

	    public function testNotExistingGbExtract()
	    {
	        $scrapper 	= new scrapper\Amazon('GB');

			$this->expectException(\Exception::class);
	       	$item 		= $scrapper->getItem('B077SF8KMG'); //ASIN updated
			
			throw new \Exception('Expected exception');
	    }

	    public function testNotExistingUsExtract()
	    {
	        $scrapper 	= new scrapper\Amazon('US');

	        $this->expectException('Exception');
	       	$item 		= $scrapper->getItem('B077SF8KMG'); //ASIN updated	
			
			throw new \Exception('Expected exception');
	    }
	}

?>